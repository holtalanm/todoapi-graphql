﻿using System;
using GraphQL;
using GraphQL.Types;
using TodoApi.Models;

namespace TodoApi.Graph.Types
{
    public class TodoItemType : ObjectGraphType<TodoItem>
    {
        public TodoItemType()
        {
            Field(item => item.Id).Description("The ID of the TodoItem");
            Field(item => item.Name).Description("The Name of the TodoItem");
            Field(item => item.IsComplete).Description("Is the Todoitem complete?");
        }
    }
}