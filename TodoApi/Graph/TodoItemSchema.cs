﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Types;
using GraphQL;
using TodoApi.Models;
using TodoApi.Graph.Queries;
using System.Text;

namespace TodoApi.Graph
{
    public class TodoItemSchema : Schema
    {
        public TodoItemSchema(TodoContext dbContext)
        {
            Query = new TodoItemQuery(dbContext);
        }
    }

    public class GraphQLQuery
    {
        public string OperationName { get; set; }
        public string NamedQuery { get; set; }
        public string Query { get; set; }
        public string Variables { get; set; }
        
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.AppendLine();
            if (!string.IsNullOrWhiteSpace(OperationName))
            {
                builder.AppendLine($"OperationName = {OperationName}");
            }
            if (!string.IsNullOrWhiteSpace(NamedQuery))
            {
                builder.AppendLine($"NamedQuery = {NamedQuery}");
            }
            if (!string.IsNullOrWhiteSpace(Query))
            {
                builder.AppendLine($"Query = {Query}");
            }
            if (!string.IsNullOrWhiteSpace(Variables))
            {
                builder.AppendLine($"Variables = {Variables}");
            }
            return builder.ToString();
        }
    }
}
