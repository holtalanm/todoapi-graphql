﻿using System;
using System.Linq;
using GraphQL;
using GraphQL.Types;
using TodoApi.Models;
using TodoApi.Graph.Types;

namespace TodoApi.Graph.Queries
{
    public class TodoItemQuery : ObjectGraphType
    {
        public TodoItemQuery(TodoContext dbContext)
        {
            Field<ListGraphType<TodoItemType>>(
                "todos",
                resolve: context => dbContext.TodoItems.ToList()
            );
        }
    }
}