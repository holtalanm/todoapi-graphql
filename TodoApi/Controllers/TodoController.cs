﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GraphQL;
using GraphQL.Types;
using TodoApi.Models;
using TodoApi.Graph;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TodoApi.Controllers
{
    [Route("/api/graphql")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        public TodoContext Context { get; }
        public TodoItemSchema Schema { get; }
        public IDocumentExecuter DocExecutor { get; }

        public TodoController(TodoContext context, TodoItemSchema schema, IDocumentExecuter documentExecutor)
        {
            Context = context;
            Schema = schema;
            DocExecutor = documentExecutor;

            if (Context.TodoItems.Count() == 0)
            {
                Context.TodoItems.Add(new TodoItem() { Name = "Item1" });
                Context.SaveChanges();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GraphQLQuery query)
        {
            if (query == null) throw new ArgumentNullException(nameof(query));

            var execOpts = new ExecutionOptions { Schema = Schema, Query = query.Query };

            try
            {
                var result = await DocExecutor.ExecuteAsync(execOpts).ConfigureAwait(false);

                if (result.Errors?.Count > 0)
                {
                    return BadRequest(result);
                }

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
